$fn=180;
union()
{
    // vertical face
    difference()
    {
        hull()
        {
            translate([-11,-1.5,-9.2])
            {    
                cube([2.5,8,33]);    
            }
            translate([-11,-8,-2.5])
            {    
                cube([2.5,0.001,2.5]);
            }
        }
        translate([-11.001,3.5,-6.2])
        {
            rotate([0,90,0])
            {
                cylinder(d=3.2,h=2.502);
            }
        }
        translate([-11.001,3.5,20.8])
        {
            rotate([0,90,0])
            {
                cylinder(d=3.2,h=2.502);
            }
        }     

    }
    // bottom mount
    difference()
    {
        hull()
        {
            translate([39,-8,-2.5])
            {
                cube([0.001,11,2.5]);
            }
            translate([-8.5,-8,-2.5])
            {
                cube([0.001,14.5,2.5]);
            }
        }
        translate([4,-4,-2.501])
        {
            cylinder(d=3.2,h=2.502);
        }
        translate([35,-4,-2.501])
        {
            cylinder(d=3.2,h=2.502);
        }    
    }
    // enforcement top
    hull()
    {
        translate([-8.5,3.5,0])
        {
            cube([0.001,3,17.5]);
        }

        translate([39,0,0])
        {
            cube([0.001,3,0.001]);
        }
    }
    // enforcement bottom
    hull()
    {
        translate([-11,-3,-7])
        {
            cube([2.5,3,4.5]);
        }
        translate([0,-3,-2.5])
        {
            cube([0.001,3,0.001]);
        }
    }
}